<?php

namespace App\Http\Controllers;

use App\Entities\Category;
use App\Http\Requests\CategoryRequest;
use App\Repositories\CategoryRepository;
use App\Services\CategoryService;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    private $categories;
    private $categoryService;

    public function __construct(CategoryRepository $categories, CategoryService $categoryService)
    {
        $this->authorizeResource(Category::class);
        $this->categories = $categories;
        $this->categoryService = $categoryService;
    }

    public function index(Request $request)
    {
        $this->authorize('view', Category::class);
        $categories = $this->categories->getTable($request->all())->get();
        return view('categories.index', compact('categories'));
    }

    public function show(Category $category)
    {
        $courses = $category->courses()->isVisible()->enrolled(auth()->id())->get();
        return view('categories.view', compact('category', 'courses'));
    }

    public function list(Request $request)
    {
        $this->authorize('create', Category::class);
        $categories = $this->categories->getTable($request->all())->paginate(20);
        return view('categories.list', compact('categories'));
    }

    public function create()
    {
        $category = new Category();
        return view('categories.create', compact('category'));
    }

    public function store(CategoryRequest $request)
    {
        $data = $request->validated();
        if ($category = $this->categoryService->create($data)) {
            return redirect(route('categories.edit', [$category->id]))->with('success', 'notification.category.create');
        }
        return back();
    }

    public function edit(Category $category)
    {
        return view('categories.edit', compact('category'));
    }

    public function update(CategoryRequest $request, Category $category)
    {
        $data = $request->validated();
        if ($this->categoryService->update($category, $data)) {
            return redirect(route('categories.edit', [$category->id]))->with('success', 'notification.category.update');
        }
        return back();
    }

    public function destroy(Category $category)
    {
        $this->categoryService->delete($category);
        return back()->with('success', 'notification.category.delete');
    }
}
