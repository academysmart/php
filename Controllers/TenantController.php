<?php

namespace App\Http\Controllers;

use App\Entities\Course;
use App\Entities\Tenant;
use App\Http\Requests\TenantRequest;
use App\Repositories\TenantRepository;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class TenantController extends Controller
{
    use AuthorizesRequests, ValidatesRequests;

    private $tenants;

    public function __construct(TenantRepository $tenants)
    {
        $this->authorizeResource(Tenant::class);
        $this->tenants = $tenants;
    }

    public function index(Request $request)
    {
        $this->authorize('view', Tenant::class);
        $tenants = $this->tenants->getTable($request->all())->paginate(20);
        return view('tenants.index', compact('tenants'));
    }

    public function show(Tenant $tenant)
    {
        return view('tenants.show', [
            'users' => $tenant->users->count(),
            'companies' => $tenant->parentGroups->count(),
            'departments' => $tenant->childGroups->count(),
            'courses' => Course::count(),
            'activeCourses' => Course::where('is_active', true)->count()
        ]);
    }

    public function create()
    {
        $tenant = new Tenant();
        return view('tenants.create', compact('tenant'));
    }

    public function store(TenantRequest $request)
    {
        if ($tenant = $this->tenants->create($request->validated())) {
            return redirect(route('tenants.edit', [$tenant->id]))->with('success', 'notification.tenant.create');
        }
        return back();
    }

    public function edit(Tenant $tenant)
    {
        return view('tenants.edit', compact('tenant'));
    }

    public function update(TenantRequest $request, Tenant $tenant)
    {
        if ($this->tenants->update($tenant, $request->validated())) {
            return redirect(route('tenants.edit', [$tenant->id]))->with('success', 'notification.tenant.update');
        }
        return back();
    }

    public function destroy(Tenant $tenant)
    {
        $this->tenants->delete($tenant);
        return back()->with('success', 'notification.tenant.delete');
    }
}
