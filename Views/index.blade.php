@extends('layouts.admin')

@section('content')
    <section class="content-header">
        <h1>
            {{ __('courses.courses') }}
        </h1>
        @include('layouts._breadcrumb', [
            'elements' => [
                ['name' => __('courses.courses'), 'url' => route_instance('courses.index')]
            ]
        ])
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ __('courses.courses') }}</h3>
                        <div class="pull-right form-inline">
                            <a class="btn btn-info btn-fill btn-wd" role="button" data-toggle="collapse" href="#collapse-search">
                                <i class="ion-ios-settings-strong"></i>
                                {{ __('buttons.search') }}
                            </a>
                            @can('courses.manage')
                            <a href="{{ route_instance('courses.create') }}" type="button" class="btn btn-success btn-fill btn-wd">{{ __('buttons.create') }}</a>
                            @endcan
                        </div>
                    </div>
                    <div class="box-body collapse{{ !empty(request()->except(['page', 'sort', 'order'])) ? ' in' : '' }}"
                         id="collapse-search">
                        @include('courses._search')
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>{{ __('courses.title') }}</th>
                                <th>{{ __('courses.description') }}</th>
                                <th>{{ __('courses.category') }}</th>
                                <th>{{ __('courses.is_active') }}</th>
                                <th>{{ __('courses.start_at') }}</th>
                                <th>{{ __('courses.end_at') }}</th>
                                <th>{{ __('courses.created_at') }}</th>
                                @can('courses.manage')
                                <th style="width: 295px">{{ __('courses.action') }}</th>
                                @endcan
                            </tr>
                            </thead>
                            <tbody>
                            @each('courses._item', $courses, 'course')
                            </tbody>
                        </table>
                    </div>
                    <div class="pull-right">
                        {!! $courses->appends(request()->except('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div>

    </section>
@endsection