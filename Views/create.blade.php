@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            {{ __('courses.add_course') }}
        </h1>
        @include('layouts._breadcrumb', [
            'elements' => [
                ['name' => __('courses.courses'), 'url' => route_instance('courses.index')],
                ['name' => __('courses.add_course'), 'url' => route_instance('courses.create')]
            ]
        ])
    </section>
    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ __('courses.add_course') }}</h3>
                    </div>
                    <form method="POST" action="{{ route('courses.store') }}" enctype="multipart/form-data">

                        @include('courses._form')

                        <div class="box-footer">
                            <button type="submit" class="btn btn-success btn-fill btn-wd">{{ __('buttons.create') }}</button>
                            <a href="{{ route('courses.index') }}" type="button" class="btn btn-default btn-fill btn-wd">{{ __('buttons.cancel') }}</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
