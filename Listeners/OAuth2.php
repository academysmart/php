<?php

namespace Modules\OAuth2\Listeners;

use Modules\OAuth2\Events\OAuth2Login;
use App\Entities\User;
use Illuminate\Support\Facades\Auth;

class OAuth2
{
    /**
     * Handle the event.
     *
     * @param object OAuth2Login $event
     * @return void
     */
    public function handle(OAuth2Login $event): void
    {
        $user = $event->user;
        $saml2User = User::firstOrNew(['email' => $user->email]);
        if (!$saml2User->id) {
            $saml2User->first_name = $user->user['given_name'] ?? $user->name;
            $saml2User->last_name = $user->user['family_name'] ?? '';
            $saml2User->setEmailVerified();
            $saml2User->save();
        }
        if (isset($saml2User)) {
            Auth::login($saml2User, true);
        }
    }
}
