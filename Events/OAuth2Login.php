<?php

namespace Modules\OAuth2\Events;

use Laravel\Socialite\Two\User;

class OAuth2Login
{
    public $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }
}
