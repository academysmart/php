<?php

namespace App\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class Group
 *
 * @property int $id
 * @property string $name
 * @property int $tenant_id
 * @property int $parent_id
 * @property int $owner_id
 * @property string $styles
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 */
class Group extends Model implements Context
{
    protected $table = 'groups';
    protected $fillable = ['name', 'tenant_id', 'owner_id', 'parent_id', 'styles'];

    protected $searchable = ['name'];

    protected $casts = [
        'styles' => 'array'
    ];

    public function owner()
    {
        return $this->belongsTo(User::class);
    }

    public function tenant()
    {
        return $this->belongsTo(Tenant::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'group_user', 'group_id', 'user_id');
    }

    public function children()
    {
        return $this->hasMany(Group::class, 'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo(Group::class, 'parent_id');
    }

    public function courses()
    {
        return $this->belongsToMany(Course::class);
    }

    public function isParent(): bool
    {
        return null === $this->parent_id;
    }

    public function scopeGetByContextId(Builder $query, int $contextId): Builder
    {
        return $query->where('parent_id', $contextId)
            ->orWhere(function (Builder $query) use ($contextId) {
                $query->where('id', $contextId)->whereNotNull('parent_id');
            });
    }

    public function scopeByTenantId(Builder $query, int $contextId): Builder
    {
        return $query->where('tenant_id', $contextId);
    }

    public function scopeEnrolled(Builder $query, int $courseId): Builder
    {
        return $query
            ->whereHas('courses', function (Builder $query) use ($courseId) {
                $query->where('id', $courseId);
            });
    }

    public function scopeNotEnrolled(Builder $query, int $courseId): Builder
    {
        return $query
            ->whereDoesntHave('courses', function (Builder $query) use ($courseId) {
                $query->where('id', $courseId);
            });
    }
}
