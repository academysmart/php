<?php

namespace App\Entities;

use Carbon\Carbon;

/**
 * Class Categories
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property int $tenant_id
 * @property int $parent_id
 * @property int $group_id
 * @property string $image
 * @property string $video
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 */
class Category extends Model
{
    protected $fillable = ['title', 'description', 'tenant_id', 'group_id', 'parent_id', 'image', 'video'];

    protected $searchable = ['title'];

    public function tenant()
    {
        return $this->belongsTo(Tenant::class);
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    public function courses()
    {
        return $this->hasMany(Course::class);
    }
}
